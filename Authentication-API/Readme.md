Routes => PUT
/register
input: username, email, password, confirm_password, table, token
correct output: status code 200 {"msg": "Registered Successfully"}

/change-password
input: username, password, table, token
correct output: status code 200 {"msg": "Password Changed"}

/activate
input: username, key, table, token
correct output: status code 200 {"msg": "Account Activated"}

Routes => POST
/authenticate
input: username, table, token
correct output: status code 200 {"msg": "Verified"}

/login
input: username, password, table, token
correct output: status code 200 {"msg": "a 36 digit userid"}


Routes => GET
/generate-token
correct output: status code 200 {"msg": "a token will generate"}

Routes => DELETE
/delete
input: username, table, token
correct output: status code 200 {"msg": "User Deleted"}
