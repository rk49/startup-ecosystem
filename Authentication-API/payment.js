const Insta = require('instamojo-nodejs');

const apiKey = 'test_5c7b411dd0792e2a35d826f5b02';
const authKey = 'test_c28882704a99bd9ac521bd73351';
Insta.setKeys(apiKey, authKey);

const data = new Insta.PaymentData();
Insta.isSandboxMode(true);

data.purpose = 'Test'; // REQUIRED
data.amount = 9; // REQUIRED
data.buyer_name = 'Rajiv';
data.email = 'rajiv.krishna@mountblue.io';
data.phone = '9897899413';
data.send_email = true;
data.webhook = 'http://www.example.com/webhook';
data.send_sms = true;
data.allow_repeated_payments = false;
const REDIRECT_URL = 'http://www.google.com';
data.setRedirectUrl(REDIRECT_URL);

Insta.createPayment(data, (error, response) => {
  if (error) {
    console.log(error);
  } else {
    // Payment redirection link at response.payment_request.longurl
    console.log(response);
  }
});
