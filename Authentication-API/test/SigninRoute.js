import request from 'supertest';
import app from '../src/app';
import config from '../src/util/config';

describe('Testing User Signin', () => {
  it('Testing login with correct credentials', (done) => {
    request(app).post('/sign')
      .send({ username: 'rajiv', password: 'rajeev', token: config.testToken })
      .expect(200)
      .expect('Valid User')
      .end(done);
  });
  it('Testing login with incorrect credentials', (done) => {
    request(app).post('/sign')
      .send({ username: 'rajiv', password: 'uhksda', token: config.testToken })
      .expect(200)
      .expect('Invalid')
      .end(done);
  });
});
