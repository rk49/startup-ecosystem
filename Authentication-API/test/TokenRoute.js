import request from 'supertest';
import chai from 'chai';
import app from '../src/app';
import verifyToken from '../src/controllers/verifyToken';
import config from '../src/util/config';

/* eslint prefer-destructuring: 0 */
const expect = chai.expect;

describe('Testing Generating Token Route', () => {
  it('Testing Generating token', (done) => {
    request(app).get('/generate-token')
      .expect(200)
      .end(done);
  });
});
describe('Testing Verifying Token ', () => {
  it('Testing token with incorrect token', async () => {
    const user = await verifyToken('token');
    const result = 'Fake Token';
    expect(user).deep.equals(result);
  });
  it('Testing token with correct token', async () => {
    const token = config.testToken;
    const user = await verifyToken(token);
    const result = 'Verified';
    expect(user).deep.equals(result);
  });
});
