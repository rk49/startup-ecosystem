import request, { agent as _agent } from 'supertest';
import app from '../src/app';
import config from '../src/util/config';

describe('Testing Register Route', () => {
  it('Testing Register with user already present', (done) => {
    request(app).put('/register')
      .send({
        username: 'jayanth', email: 'jayanths122@gmail.com', password: '123456', confirm_password: '123456', token: config.token.testToken,
      })
      .expect(500)
      .expect('User Exists')
      .end(done);
  });
  const agent = _agent(app);
  beforeEach((done) => {
    agent.delete('/delete')
      .send({ username: 'testing1' })
      .expect(200)
      .end(done);
  });
  it('Testing Register using creating new user', (done) => {
    agent.put('/register')
      .send({
        name: 'Rajeev', username: 'testing1', email: 'rk@gmail', sex: 'Male', password: 'rajeev', confirm_password: 'rajeev', token: config.testToken,
      }).expect(200)
      .expect('Registered Successfully')
      .end(done);
  });
});
