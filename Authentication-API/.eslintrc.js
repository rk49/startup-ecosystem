module.exports = {
    "extends": "airbnb",
    "env": {
        "jasmine": true
    },
    "rules": {
        "no-param-reassign": 0,
        "no-restricted-syntax": 0
      }
};