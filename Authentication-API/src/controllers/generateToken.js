import { sign } from 'jsonwebtoken';
import config from '../util/config';

function generateToken() {
  try {
    const token = sign({
      token: config.token.tokenData,
    },
    config.token.secretKey,
    {
      expiresIn: '10000d',
    });
    return token;
  } catch (err) { throw (err); }
}
export default generateToken;
