import uuidv4 from 'uuid/v4';
import { hash, compare } from 'bcryptjs';
import axios from 'axios';
import { info } from '../winston/config';
import fetchSqlData from '../util/connection';
import config from '../util/config';

const APIkey = 'f46bfe5081d2a2309ab9f1cdbebfdfeeeddc273afe735725daa0edd5f391';
const emailverification = require('quickemailverification').client(APIkey).quickemailverification();

function verifyEmail(emailid) {
  return new Promise(((res, reject) => {
    emailverification.verify(emailid, (err, response) => {
      if (err) {
        reject(err);
      }
      res(response.body);
    });
  }));
}
async function deleteUser(user) {
  try {
    info('Delete User');
    let query = `Select * from ${user.table} where uid='${user.uid}';`;
    let result = await fetchSqlData(query);
    info(result);
    if (result.length > 0) {
      query = `Delete from ${user.table} where uid='${user.uid}';`;
      result = await fetchSqlData(query);
      if (result.affectedRows > 0) {
        info('User deletedd');
        return 'User Deleted';
      }
      info('User not exists');
    }
    return `No user present with the uid ${user.uid}`;
  } catch (error) {
    info(error);
    throw (error);
  }
}
async function register(user) {
  try {
    info('Register');
    if (user.password === user.confirm_password) {
      const pass = await hash(user.password, 9);
      user.password = pass;
      user.confirm_password = pass;
      info('Password Matched');
      let query = `Select * from ${user.table} where username='${user.username}' OR email='${user.email}'`;
      let result = await fetchSqlData(query);
      info(result);
      if (result.length > 0) {
        info('Already user present');
        return 'User Exists';
      }
      const resp = await verifyEmail(user.email);
      if (resp.result !== 'valid') { return 'Email id does not valid'; }
      const uid = uuidv4();
      query = `INSERT INTO ${user.table} (uid, password, username, email, status) VALUES ('${uid}', '${user.password}', '${user.username}', '${user.email}', 'unauthenticated');`;
      result = await fetchSqlData(query);
      if (result.affectedRows > 0) {
        const url = `${config.ec2url}:5001/send-email`;
        const text = `Your username is ${user.username} <br>To activate account tap on the link and fill the form <br><br><br><br> ${config.ec2url}:3000/users/registerdetails/${uid}`;
        const body = { to: user.email, subject: 'Startup Ecosystem E-mail Verification', body: text };
        result = await axios.post(url, body);
        if (result.data === 'Mail sent') { return 'Registered Successfully'; }
        deleteUser(user);
        return 'Something went wrong. Try again later';
      }
      return 'Something went wrong';
    }
    return 'Password not matched';
  } catch (error) {
    info(error);
    return (error);
  }
}


async function login(user) {
  try {
    info('login');
    const query = `Select * from ${user.table} where username='${user.username}' OR email='${user.username}'`;
    const result = await fetchSqlData(query);
    info(result);
    if (result.length > 0) {
      info('User id present');
      if (await compare(user.password, result[0].password)) {
        info('Valid User');
        return `${result[0].uid},${result[0].status}`;
      }
    }
    return 'Invalid User';
  } catch (err) {
    info(err);
    return (err);
  }
}
async function activate(user) {
  try {
    if (user.msg === 'OK') {
      let query = `Select * from ${user.table} where uid='${user.uid}'`;
      const result = await fetchSqlData(query);
      if (result.length > 0) {
        query = `UPDATE ${user.table} SET status='Authenticated' WHERE uid='${user.uid}'`;
        await fetchSqlData(query);
        return 'Account Activated';
      }
      return 'No User Exists';
    }
    return 'User needs to register the form';
  } catch (error) {
    return error;
  }
}
async function changePass(user) {
  try {
    let query = `Select * from ${user.table} where uid='${user.uid}';`;
    const result = await fetchSqlData(query);
    info(result);
    if (result.length > 0) {
      info('User id present');
      if (await compare(user.password, result[0].password)) {
        const password = await hash(user.newPassword, 9);
        query = `UPDATE ${user.table} SET password='${password}' WHERE uid='${user.uid}'`;
        await fetchSqlData(query);
        return 'Password Changed';
      }
    }
    return 'Try again later';
  } catch (error) {
    return error;
  }
}
async function toCheckUserId(user) {
  try {
    const query = `Select * from ${user.table} where uid='${user.uid}'`;
    const result = await fetchSqlData(query);
    if (result.length > 0) {
      return 'User Exist';
    }
    return 'No User Exists';
  } catch (error) {
    return error;
  }
}
// async function authenticate(user) {
//   try {
//     const query = `Select * from ${user.table} where username=
// '${user.username}' OR email='${user.username}'`;
//     const result = await fetchSqlData(query);
//     if (result.length > 0) {
//       if (result[0].status === 'verified') {
//         return 'Verified';
//       }
//       return 'User needs to activate his account';
//     }
//     return 'No User Exists';
//   } catch (error) {
//     return error;
//   }
// }
export default {
  login, register, deleteUser, activate, changePass, toCheckUserId,
};
