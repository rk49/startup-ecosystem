import { verify } from 'jsonwebtoken';
import config from '../util/config';

async function verifyToken(token) {
  try {
    const user = await verify(token, config.token.secretKey);
    if (user.exp > 0) {
      if (user.token === config.token.tokenData) {
        return 'Verified';
      }
      return 'Invalid Token';
    }
    return 'Token Expired Generate a new token and Try Again';
  } catch (err) { return ('Fake Token'); }
}
export default verifyToken;
