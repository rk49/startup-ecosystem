import express from 'express';
import morgan from 'morgan';

import { urlencoded, json } from 'body-parser';
import user from './routes/user';
import token from './routes/token';
import { stream as _stream, info } from './winston/config';


const app = express();
app.use(urlencoded({ extended: true }));
app.use(json());
app.use(morgan('combined', { stream: _stream }));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  if (req.method === 'OPTIONS') {
    res.send(200);
  } else {
    next();
  }
});
app.use('/', user);
app.use('/', token);
app.listen(5003, () => {
  info('Running at port 5003');
});
export default app;
