import { Router } from 'express';

import generateToken from '../controllers/generateToken';

const router = Router();
router.route('/generate-token').get(async (req, res) => {
  const result = generateToken();
  res.send(result);
});
export default router;
