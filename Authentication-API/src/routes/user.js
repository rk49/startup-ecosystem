import { Router } from 'express';
import * as authenticate from '../controllers/authenticate';
import verifyToken from '../controllers/verifyToken';
import { info } from '../winston/config';

const router = Router();


router.route('/register').put(async (req, res) => {
  try {
    if (req.body.token === undefined) { res.status(200).json({ msg: 'Missing Token' }); }

    const user = await verifyToken(req.body.token);
    info(user);

    if (user === 'Verified') {
      const result = await authenticate.default.register(req.body);
      if (result === 'Registered Successfully') {
        res.status(200).json({ msg: result });
      } else {
        res.status(200).json({ msg: result });
      }
    } else {
      res.status(200).json({ msg: user });
    }
  } catch (error) {
    res.status(500).json({ msg: error });
  }
});
router.route('/change-password').put(async (req, res) => {
  try {
    if (req.body.token === undefined) { res.status(200).json({ msg: 'Missing Token' }); }

    const user = await verifyToken(req.body.token);
    info(user);

    if (user === 'Verified') {
      const result = await authenticate.default.changePass(req.body);
      if (result === 'Password Changed') {
        res.status(200).json({ msg: result });
      } else {
        res.status(200).json({ msg: result });
      }
    } else {
      res.status(200).json({ msg: user });
    }
  } catch (error) {
    res.status(500).json({ msg: error });
  }
});

router.route('/activate').put(async (req, res) => {
  try {
    if (req.body.token === undefined) { res.status(200).json({ msg: 'Missing Token' }); }

    const user = await verifyToken(req.body.token);
    info(user);

    if (user === 'Verified') {
      const result = await authenticate.default.activate(req.body);
      if (result === 'Account Activated') {
        res.status(200).json({ msg: result });
      } else {
        res.status(200).json({ msg: result });
      }
    } else {
      res.status(200).json({ msg: user });
    }
  } catch (error) {
    res.status(500).json({ msg: error });
  }
});

router.route('/authenticate').post(async (req, res) => {
  try {
    if (req.body.token === undefined) { res.status(200).json({ msg: 'Missing Token' }); }

    const user = await verifyToken(req.body.token);
    info(user);

    if (user === 'Verified') {
      const result = await authenticate.default.authenticate(req.body);
      if (result === 'Verified') {
        res.status(200).json({ msg: result });
      } else {
        res.status(200).json({ msg: result });
      }
    } else {
      res.status(200).json({ msg: user });
    }
  } catch (error) {
    res.status(500).json({ msg: error });
  }
});
router.route('/check-user').post(async (req, res) => {
  try {
    if (req.body.token === undefined) { res.status(200).json({ msg: 'Missing Token' }); }

    const user = await verifyToken(req.body.token);
    info(user);

    if (user === 'Verified') {
      const result = await authenticate.default.toCheckUserId(req.body);
      if (result === 'User Exist') {
        res.status(200).json({ msg: result });
      } else {
        res.status(200).json({ msg: result });
      }
    } else {
      res.status(200).json({ msg: user });
    }
  } catch (error) {
    res.status(500).json({ msg: error });
  }
});
router.route('/login').post(async (req, res) => {
  try {
    if (req.body.token === undefined) { res.status(200).json({ msg: 'Missing Token' }); }
    const user = await verifyToken(req.body.token);
    info(user);
    if (user === 'Verified') {
      const result = await authenticate.default.login(req.body);
      if (result.length === 36) { res.status(200).json({ msg: result }); } else {
        res.status(200).json({ msg: result });
      }
    } else {
      res.status(200).json({ msg: user });
    }
  } catch (error) {
    res.status(500).json({ msg: error });
  }
});

router.route('/delete').delete(async (req, res) => {
  try {
    const result = await authenticate.default.deleteUser(req.body);
    if (result === 'User Deleted') { res.status(200).json({ msg: result }); } else {
      res.status(200).json({ msg: result });
    }
  } catch (error) {
    res.status(500).json({ msg: error });
  }
});

export default router;
