import { createConnection } from 'mysql2/promise';
import config from './config';

let configuration;
if (config.mode === 'production') {
  configuration = config.configProduction;
} else {
  configuration = config.configDev;
}
async function getConnection() {
  try {
    const connection = await createConnection(configuration);
    return connection;
  } catch (err) {
    throw (err);
  }
}
async function fetchSqlData(sql) {
  try {
    const conn = await getConnection();
    const result = await conn.execute(sql);
    conn.end();
    return (result[0]);
  } catch (err) { throw err; }
}
export default fetchSqlData;
