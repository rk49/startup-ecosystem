module.exports = {
  dropInvestors: "DROP TABLE IF EXISTS investors",
  dropUsers: "DROP TABLE IF EXISTS users",
  insertIntoInvestors: `INSERT INTO investors(id, inv_uid, inv_first, inv_last, inv_email, inv_sex, 
    inv_country, inv_state, inv_city, inv_pincode, inv_linkedin, inv_interest, inv_investment, inv_phone_no, 
    inv_pan, inv_aadhaar, inv_dob, inv_pic) VALUES ?`,
  insertIntoUsers: `INSERT INTO users(id, uid, first_name, last_name, email, sex, address,
    country, state, city, dob, pan, aadhaar, phone_no, pincode) VALUES ?`,
};
