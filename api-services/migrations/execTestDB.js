const pool = require('../src/config/connection');
const { insertIntoInvestors, insertIntoUsers } = require('./queries');

const invObj = [[null, "uid1", "first_name", "last_name", "email1", "male", "India", "Karnataka", "Bengaluru", 560003, "jay@linkedin.io", "IT", "55,000", 850812125, "pan_number", "Aadhar number", "23/07/15", "profile_pic"]];

const userObj = [[null, "uid1", "first", "last", "email1", "male", "address", "India", "Karnataka", "Bengaluru", "23-07-15", "pan", "aadhar", 398333439, 23213]];

async function execDB() {
  const invDeleted = await pool.query('DELETE FROM investors');
  const usrDeleted = await pool.query('DELETE FROM users');
  const usrIdeas = await pool.query('DELETE FROM user_ideas');
  const shrIdeas = await pool.query('DELETE FROM shared_ideas');
  if (invDeleted && usrDeleted && usrIdeas && shrIdeas) {
    const insertedInv = await pool.query(insertIntoInvestors, [invObj]);
    const insertedUsr = await pool.query(insertIntoUsers, [userObj]);
    if (insertedInv && insertedUsr) {
      console.log('Configured DB');
      pool.end();
    }
  }
}

execDB();
