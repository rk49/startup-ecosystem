const request = require('supertest');
const app = require('../server');
const { JWT_TOKEN } = require('../src/config/config');

require('./submitIdeaRouteTest');

describe('Basic Supertest for USER route route', () => {
  it('should be able to access protected /users/getUserById route ', function (done) {
    this.timeout(10000);
    request(app)
      .post('/users/getUserById')
      .send({ uid: "uid1", token: JWT_TOKEN })
      .expect(200)
      .end(done);
  });
  it('should not be able to access protected /users/getUserByIdroute without token', function (done) {
    this.timeout(10000);
    request(app)
      .post('/users/getUserById')
      .send({ uid: "uid1" })
      .expect(401)
      .end(done);
  });

  it('should not be able to access protected /users/getUserById route for non existant users', function (done) {
    this.timeout(10000);
    request(app)
      .post('/users/getUserById')
      .send({ uid: "uid10", token: JWT_TOKEN })
      .expect(500)
      .end(done);
  });
});
