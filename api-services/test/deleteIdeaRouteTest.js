const request = require('supertest');
const app = require('../server');
const { getIdeas } = require('../src/controller/ideasController');
const { JWT_TOKEN } = require('../src/config/config');

require('./disapproveIdeaRouteTest');

describe('Basic Supertest for delete ideas route', async () => {
    const ideasObj = await getIdeas();
    it('should be able to access protected /ideas/disapproveIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/deleteIdea')
          .send({ idea_id: ideasObj.ideas[0].id, token: JWT_TOKEN  })
          .expect(200)
          .end(done);
      });
        it('should not be able to access protected /ideas/disapproveIdearoute ', function (done) {
          this.timeout(10000);
          request(app)
            .post('/ideas/deleteIdea')
            .send({ idea_id: ideasObj.ideas[1].id, token: JWT_TOKEN  })
            .expect(500)
            .end(done);
        });
    
      it('should not be able to access protected /ideas/disapproveIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/deleteIdeaWrongURL')
          .send({ idea_id: ideasObj.ideas[0].id, token: JWT_TOKEN  })
          .expect(404)
          .end(done);
      });
});