const { expect } = require('chai');
const { disapproveIdea, getIdeas } = require('../src/controller/ideasController');

require('./approveIdeaControllerTest');

describe('Basic MOCHA test for disapprove ideas controller', () => {
  it('Should disapprove idea specified by investor', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const ideasObj = await getIdeas();
    const result = await disapproveIdea({ idea_id: ideasObj.ideas[0].id, inv_uid: 'uid1' });
    const actualResult = result.disapproved;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail to disapprove idea to non existing idea', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await disapproveIdea({ uid: "uid2", inv_uid: 'uid10' });
    const actualResult = result.disapproved;
    expect(expectedResult).deep.equals(actualResult);
  });
});
