const request = require('supertest');
const app = require('../server');
const { JWT_TOKEN } = require('../src/config/config');

describe('Basic Supertest for get Ideas route', () => {
  it('should be able to access protected ideas/getideas route ', function (done) {
    this.timeout(10000);
    request(app)
      .post('/ideas/getIdeas')
      .send({ token: JWT_TOKEN })
      .expect(200)
      .end(done);
  });

  it('should be able to access protected ideas/getideasroute ', function (done) {
    this.timeout(10000);
    request(app)
      .post('/ideas/getIdeasWrongURL')
      .send({ token: JWT_TOKEN })
      .expect(404)
      .end(done);
  });
});
