const { expect } = require('chai');
const { getUserById } = require('../src/controller/usersController');

describe('Basic MOCHA test for users controller', () => {
  it('Should return users in DB by UID', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const result = await getUserById('uid1');
    const actualResult = result.fetched;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail to return users in DB by UID', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await getUserById('uid100');
    const actualResult = result.fetched;
    expect(expectedResult).deep.equals(actualResult);
  });
});
