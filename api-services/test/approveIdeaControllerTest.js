const { expect } = require('chai');
const { approveIdea, getIdeas } = require('../src/controller/ideasController');

require('./submitIdeaControllerTest');

describe('Basic MOCHA test for approve ideas controller', () => {
  it('Should approve idea specified by investor', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const ideasObj = await getIdeas();
    const result = await approveIdea({ idea_id: ideasObj.ideas[0].id, inv_uid: 'uid1' });
    const actualResult = result.approved;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail approve idea to non existing idea', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await approveIdea({ uid: "uid2", inv_uid: 'uid10' });
    const actualResult = result.approved;
    expect(expectedResult).deep.equals(actualResult);
  });
});
