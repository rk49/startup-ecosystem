const request = require('supertest');
const app = require('../server');
const { getIdeas } = require('../src/controller/ideasController');
const { JWT_TOKEN } = require('../src/config/config');

require('./registerIdeaRouteTest');

const idea = {
    inv_uid: "uid1",
    status: "pending",
  };  

describe('Basic Supertest for register Idea route', async () => {
    const ideasObj = await getIdeas();
    it('should be able to access protected /ideas/submitIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/submitIdea')
          .send({ ...idea, idea_id: ideasObj.ideas[0].id, token: JWT_TOKEN  })
          .expect(200)
          .end(done);
      });
        it('should not be able to access protected /ideas/submitIdearoute ', function (done) {
          this.timeout(10000);
          request(app)
            .post('/ideas/submitIdea')
            .send({ ...idea, id: 10, token: JWT_TOKEN  })
            .expect(500)
            .end(done);
        });
    
      it('should not be able to access protected /ideas/submitIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/submitIdeaWrongURL')
          .send({ ...idea, token: JWT_TOKEN })
          .expect(404)
          .end(done);
      });
});