const { expect } = require('chai');
const { registerIdea } = require('../src/controller/ideasController');

const idea = {
  uid: "uid1",
  title: "First title",
  budget: "50000",
  status: "pending",
  members: 10,
  description: "desc1",
  docs: "docs1",
};

describe('Basic MOCHA test for ideas controller', () => {
  it('Should register idea in DB', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const result = await registerIdea(idea);
    const actualResult = result.inserted;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should register idea2 in DB', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const result = await registerIdea({...idea, docs: "docsNew"});
    const actualResult = result.inserted;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail to register idea in DB for same creds', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await registerIdea(idea);
    const actualResult = result.inserted;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail to register idea in DB for wrong creds', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await registerIdea({ ...idea, uid: "uid2" });
    const actualResult = result.inserted;
    expect(expectedResult).deep.equals(actualResult);
  });
});
