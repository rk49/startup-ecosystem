const request = require('supertest');
const app = require('../server');
const { JWT_TOKEN } = require('../src/config/config');

require('./submitIdeaRouteTest');

describe('Basic Supertest for getIdeasByInvUidRoute route', () => {
    it('should be able to access protected /ideas/getIdeasByInvId route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/getIdeasByInvId')
          .send({inv_uid: "uid1", token: JWT_TOKEN })
          .expect(200)
          .end(done);
      });
        it('should not be able to access protected /ideas/getIdeasByInvIdroute ', function (done) {
          this.timeout(10000);
          request(app)
            .post('/ideas/getIdeasByInvId')
            .send({inv_uid: "uid10", token: JWT_TOKEN })
            .expect(500)
            .end(done);
        });
    
      it('should not be able to access protected /ideas/getIdeasByInvId route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/getIdeasByInvIdWrongURL')
          .send({inv_uid: "uid1", token: JWT_TOKEN })
          .expect(404)
          .end(done);
      });
});