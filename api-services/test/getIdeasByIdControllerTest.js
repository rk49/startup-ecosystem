const { expect } = require('chai');
const { getIdeasByInvId } = require('../src/controller/ideasController');
require('./submitIdeaControllerTest');

describe('Basic MOCHA test for ideas controller', () => {
  it('Should return ideas in DB by investor ID', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const result = await getIdeasByInvId('uid1');
    const actualResult = result.fetched;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail to return ideas in DB by investor ID', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await getIdeasByInvId('uid10');
    const actualResult = result.fetched;
    expect(expectedResult).deep.equals(actualResult);
  });
});
