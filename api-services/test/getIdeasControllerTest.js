const { expect } = require('chai');
const { getIdeas } = require('../src/controller/ideasController');
require('./registerIdeaControllerTest');

describe('Basic MOCHA test for ideas controller', () => {
  it('Should return ideas in DB', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const result = await getIdeas();
    const actualResult = result.fetched;
    expect(expectedResult).deep.equals(actualResult);
  });
});
