const { expect } = require('chai');
const { submitIdea, getIdeas } = require('../src/controller/ideasController');

require('./registerIdeaControllerTest');

const idea = {
  inv_uid: "uid1",
  status: "pending",
};

describe('Basic MOCHA test for ideas controller', () => {
  it('Should submit idea to specified investor', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const ideasObj = await getIdeas();
    const result = await submitIdea({ ...idea, idea_id: ideasObj.ideas[0].id });
    const actualResult = result.inserted;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail submit idea to non existing investor', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await submitIdea({ ...idea, uid: "uid2" });
    const actualResult = result.inserted;
    expect(expectedResult).deep.equals(actualResult);
  });
});
