const { expect } = require('chai');
const { getInvestors } = require('../src/controller/investorsController');

describe('Basic MOCHA test for investors controller', () => {
  it('Should return investors in DB', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const result = await getInvestors();
    const actualResult = result.fetched;
    expect(expectedResult).deep.equals(actualResult);
  });
});
