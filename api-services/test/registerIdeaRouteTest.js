const request = require('supertest');
const app = require('../server');
const { JWT_TOKEN } = require('../src/config/config');

require('./disapproveIdeaControllerTest');

const idea = {
  uid: "uid1",
  title: "Second title",
  budget: "50000",
  status: "pending",
  members: 10,
  description: "desc2",
  docs: "docs2",
};

describe('Basic Supertest for register Idea route', () => {
    it('should be able to access protected /ideas/registerIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/registerIdea')
          .send({...idea, token: JWT_TOKEN })
          .expect(200)
          .end(done);
      });

      it('should be able to access protected /ideas/registerIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/registerIdea')
          .send({...idea, docs: "anotherDoc", token: JWT_TOKEN })
          .expect(200)
          .end(done);
      });

        it('should not be able to access protected /ideas/registerIdearoute ', function (done) {
          this.timeout(10000);
          request(app)
            .post('/ideas/registerIdea')
            .send({ ...idea, uid: "uid2", token: JWT_TOKEN  })
            .expect(500)
            .end(done);
        });
    
      it('should not be able to access protected /ideas/registerIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/registerIdeaWrongURL')
          .send({ ...idea, token: JWT_TOKEN  })
          .expect(404)
          .end(done);
      });
});