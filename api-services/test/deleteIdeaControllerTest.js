const { expect } = require('chai');
const { deleteIdea, getIdeas } = require('../src/controller/ideasController');

require('./disapproveIdeaControllerTest');

describe('Basic MOCHA test for delete ideas controller', async () => {
  const ideasObj = await getIdeas();
  it('Should delete idea specified by investor', async function () {
    this.timeout(10000);
    const expectedResult = true;
    const result = await deleteIdea(ideasObj.ideas[0].id);
    const actualResult = result.deleted;
    expect(expectedResult).deep.equals(actualResult);
  });

  it('Should fail to delete unapproved idea', async function () {
    this.timeout(10000);
    const expectedResult = false;
    const result = await deleteIdea(ideasObj.ideas[1].id);
    const actualResult = result.deleted;
    expect(expectedResult).deep.equals(actualResult);
  });

});
