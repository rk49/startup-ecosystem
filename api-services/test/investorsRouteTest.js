const request = require('supertest');
const app = require('../server');
const { JWT_TOKEN } = require('../src/config/config');

describe('Basic Supertest for Investors route', () => {
  it('should be able to access protected /investors/getInvestors route ', function (done) {
    this.timeout(10000);
    request(app)
      .post('/investors/getInvestors')
      .send({ token: JWT_TOKEN })
      .expect(200)
      .end(done);
  });

  it('should be able to access protected /investors/getInvestors route ', function (done) {
    this.timeout(10000);
    request(app)
      .post('/investors/getInvestorsWrongURL')
      .send({ token: JWT_TOKEN })
      .expect(404)
      .end(done);
  });
});
