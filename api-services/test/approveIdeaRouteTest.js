const request = require('supertest');
const app = require('../server');
const { getIdeas } = require('../src/controller/ideasController');
const { JWT_TOKEN } = require('../src/config/config');

require('./submitIdeaRouteTest');

describe('Basic Supertest for approve ideas route', async () => {
    const ideasObj = await getIdeas();
    it('should be able to access protected /ideas/approveIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/approveIdea')
          .send({ idea_id: ideasObj.ideas[0].id, inv_uid: 'uid1', token: JWT_TOKEN })
          .expect(200)
          .end(done);
      });
        it('should not be able to access protected /ideas/approveIdearoute ', function (done) {
          this.timeout(10000);
          request(app)
            .post('/ideas/approveIdea')
            .send({ idea_id: ideasObj.ideas[0].id, inv_uid: 'uid10', token: JWT_TOKEN  })
            .expect(500)
            .end(done);
        });
    
      it('should not be able to access protected /ideas/approveIdea route ', function (done) {
        this.timeout(10000);
        request(app)
          .post('/ideas/approveIdeaWrongURL')
          .send({ idea_id: ideasObj.ideas[0].id, inv_uid: 'uid1', token: JWT_TOKEN })
          .expect(404)
          .end(done);
      });
});