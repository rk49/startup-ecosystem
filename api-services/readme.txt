 -> Initial instruction - 
1)npm install -g nodemon
2)npm run lint-setup
3)npm install 

4)Copy paste this in .eslintrc.json = 
{
    "extends": "airbnb",
    "rules" : {
        "linebreak-style": 0,
        "quotes": 0,
        "no-console": 0,
        "func-names": 0,
        "camelcase": 0,
        "prefer-arrow-callback": 0
  },
  "env": {
    "node": true,
    "mocha": true
  }
}

5)npm start
 
 
 Available APIS - 
 (Dev: host = localhost, PORT = 4000)
________________________________________________________________________________________________________

b)Ideas APIS - 

1)"http://HOST:PORT/ideas/getIdeas" - Returns all the ideas registered with the DB
Description: 
-> GET request
-> Sample response = {
    "ideas": [
        {
            "id": 2,
            "uid": "uid1",
            "title": "First title",
            "budget": "50000",
            "status": "pending",
            "members": 10,
            "description": "desc1",
            "docs": "docs1"
        }
    ],
    "msg": "Ideas successfully fetched"
}	
-> STATUS CODES - 200, 500, 404


2)"http://HOST:PORT/ideas/getIdeasByInvId(inv_uid)" - Returns the ideas of investors specified by their uid
 -> POST request
 -> Sample request object = 
 {
     inv_uid: "uid3
 }
 -> STATUS CODES - 200, 500, 404


 3)"http://HOST:PORT/ideas/registerIdea" - Registers the user's idea to the DB but not yet submitted to the investor
 -> POST request
 -> Sample request object = 
 {
	 "uid": "uid1", 
	 "title": "Another title", 
	 "budget": "50000", 
	 "status": "pending", 
	 "members": 10, 
	 "description": "desc", 
	 "docs": "docs"
}
-> Sample response = {
    "inserted": true,
    "msg": "Idea successfully registered with the database"
}
-> STATUS CODES - 200, 500, 404


4)"http://HOST:PORT/ideas/submitIdea" - Submits the existing idea to the specified investor
-> POST request
-> Sample request object = 
{
	"idea_id": 4, 
	"inv_uid": "uid3", 
	"status": "pending"
}
->Sample response = {
    "inserted": true,
    "msg": "Idea successfully submitted to the specified investor"
}
-> STATUS CODES - 200, 500, 404

5)"http://HOST:PORT/ideas/approveIdea" - Approves idea specified by investor uid
-> POST request
-> Sample request object = 
{
	"idea_id": 1,
	"inv_uid": "uid1",
	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw"
}
-> Sample response = {
    "approved": false,
    "msg": "Something went wrong. Idea could not be approved"
}
-> STATUS CODES - 200, 500, 404



6)"http://HOST:PORT/ideas/disapproveIdea" - Disapproves idea specified by investor uid
-> POST request
-> Sample request object = 
{
	"idea_id": 1,
	"inv_uid": "uid1",
	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw"
}
-> Sample response = {
    "disapproved": true,
    "msg": "Idea has been disapproved"
}
-> STATUS CODES - 200, 500, 404


7)"http://HOST:PORT/ideas/deleteIdea" - Deletes idea if it has not been approved
-> POST request
-> Sample response = {
    "deleted": true,
    "msg": "Idea successfully deleted from DB"
}
-> Sample request object = 
{
	"idea_id": 1,
	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw"
}

_______________________________________________________________________________________________________

c)Investor APIS

1)"http://HOST:PORT/investors/getInvestors" - Returns all the registered investors from DB
-> GET request
-> STATUS CODES - 200, 500, 404