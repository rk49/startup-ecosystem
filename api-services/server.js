const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
const { JWT_KEY, JWT_ID } = require('./src/config/config');

const usersRoute = require('./src/routes/usersRoute');
const ideasRouter = require('./src/routes/ideasRoute');
const investorsRoute = require('./src/routes/investorsRouter');
const tokenChecker = require('./src/services/authCheck');

const app = express();
const PORT = 4000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));

app.get('/', (req, res) => {
  const token = jwt.sign({
    id: JWT_ID,
  },
  JWT_KEY,
  {
    expiresIn: "15d",
  });
  res.send(token);
});
app.use(cors());

app.use('/investors', tokenChecker, investorsRoute);

app.use('/ideas', tokenChecker, ideasRouter);

app.use('/users', tokenChecker, usersRoute);

app.listen(PORT, () => console.log(`App listening on post ${PORT}`));

module.exports = app;
