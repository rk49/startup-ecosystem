module.exports = {
    "extends": "airbnb",
    "rules": {
      "linebreak-style": 0,
      "quotes": 0,
      "no-console": 0,
      "camelcase": 0,
      "func-names": 0,
      "prefer-arrow-callback": 0,
    },
    "env":{
       "node": true,
       "mocha": true, 
    }
};