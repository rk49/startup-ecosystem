const pool = require('../config/connection');

module.exports = {
  getInvestors: async () => {
    let investors;
    try {
      investors = await pool.query('SELECT * FROM investors');
    } catch (err) {
      return {
        fetched: false,
        msg: err,
      };
    }
    if (investors) {
      return {
        fetched: true,
        investors,
        msg: "Successfully fetched investors",
      };
    }
    return {
      fetched: false,
      msg: "Something went wrong, could not fetch investors",
    };
  },
};
