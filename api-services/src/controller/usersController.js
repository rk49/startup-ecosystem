const pool = require('../config/connection');

module.exports = {
  getUserById: async (uid) => {
    let userObj;
    try {
      userObj = await pool.query(`SELECT * FROM users WHERE uid = "${uid}"`);
    } catch (err) {
      return {
        fetched: false,
        msg: err.sqlMessage,
      };
    }
    if (userObj.length > 0) {
      return {
        fetched: true,
        user: userObj,
        msg: 'User of specified uid succesfully fetched',
      };
    }
    return {
      fetched: false,
      msg: 'There is no user of this uid',
    };
  },
};
