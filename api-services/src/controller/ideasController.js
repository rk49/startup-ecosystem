const pool = require('../config/connection');

module.exports = {
  getIdeas: async () => {
    try {
      const ideas = await pool.query(`SELECT * from user_ideas`);
      if (ideas) {
        return {
          fetched: true,
          ideas,
          msg: "Ideas successfully fetched",
        };
      }
      return {
        fetched: false,
        msg: "Something went wrong, the idea could not be fetched from the database",
      };
    } catch (err) {
      return {
        fetched: false,
        msg: err.sqlMessage,
      };
    }
  },
  getIdeasById: async (uid) => {
    try {
      const ideas = await pool.query(`SELECT * from user_ideas where uid = "${uid}"`);
      if (ideas) {
        return {
          fetched: true,
          ideas,
          msg: "Ideas successfully fetched",
        };
      }
      return {
        fetched: false,
        msg: "Something went wrong, the idea could not be fetched from the database",
      };
    } catch (err) {
      return {
        fetched: false,
        msg: err.sqlMessage,
      };
    }
  },
  getIdeasByInvId: async (inv_uid) => {
    try {
      const ideas = await pool.query(`SELECT * FROM user_ideas where id in (select idea_id from shared_ideas where inv_uid="${inv_uid}")`);
      if (ideas.length > 0) {
        return {
          fetched: true,
          ideas,
          msg: "Ideas successfully fetched",
        };
      }
      return {
        fetched: false,
        msg: "There are no ideas for this investor",
      };
    } catch (err) {
      return {
        fetched: false,
        msg: err.sqlMessage,
      };
    }
  },
  registerIdea: async (ideaInfo) => {
    const {
      uid, title, budget, status, members, description, docs,
    } = ideaInfo;
    try {
      const inserted = await pool.query(`INSERT INTO user_ideas(id, uid, title, budget, status,
      members, description, docs) VALUES (null, "${uid}", "${title}", "${budget}", "${status}",
      "${members}", "${description}", "${docs}")`);
      if (inserted.affectedRows > 0) {
        return {
          inserted: true,
          msg: "Idea successfully registered with the database",
        };
      }
      return {
        inserted: false,
        msg: "Something went wrong, the idea could not be registered with the database",
      };
    } catch (err) {
      return {
        inserted: false,
        msg: err.sqlMessage,
      };
    }
  },
  submitIdea: async (ideaInfo) => {
    const {
      idea_id, inv_uid, status,
    } = ideaInfo;
    try {
      const inserted = await pool.query(`INSERT INTO shared_ideas(id, idea_id, inv_uid, status) VALUES 
      (null, "${idea_id}", "${inv_uid}", "${status}")`);
      if (inserted.affectedRows > 0) {
        return {
          inserted: true,
          msg: "Idea successfully submitted to the specified investor",
        };
      }
      return {
        inserted: false,
        msg: "Something went wrong, the idea could not be submitted",
      };
    } catch (err) {
      return {
        inserted: false,
        msg: err.sqlMessage,
      };
    }
  },
  deleteIdea: async (id) => {
    try {
      const deleted = await pool.query(`DELETE FROM user_ideas where id = ${id} AND status != "approved"`);
      if (deleted.affectedRows > 0) {
        return {
          deleted: true,
          msg: "Idea successfully deleted from DB",
        };
      }
      return {
        deleted: false,
        msg: "Something went wrong, the idea could not be deleted",
      };
    } catch (err) {
      return {
        deleted: false,
        msg: err.sqlMessage,
      };
    }
  },
  approveIdea: async (approveObj) => {
    const { inv_uid, idea_id } = approveObj;
    let approved1; let approved2;
    try {
      approved1 = await pool.query(`UPDATE shared_ideas SET status = "approved" WHERE inv_uid = "${inv_uid}" AND idea_id = ${idea_id}`);
      approved2 = await pool.query(`UPDATE user_ideas SET status = "approved" WHERE id = ${idea_id}`);
    } catch (err) {
      return {
        approved: false,
        msg: err.sqlMessage,
      };
    }
    if (approved1.affectedRows > 0 && approved2.affectedRows > 0) {
      return {
        approved: true,
        msg: "Idea has been approved",
      };
    }
    return {
      approved: false,
      msg: "Something went wrong. Idea could not be approved",
    };
  },
  disapproveIdea: async (disapproveObj) => {
    const { inv_uid, idea_id } = disapproveObj;
    let disapproved1; let disapproved2;
    try {
      disapproved1 = await pool.query(`UPDATE shared_ideas SET status = "disapproved" WHERE inv_uid = "${inv_uid}" AND idea_id = ${idea_id}`);
      disapproved2 = await pool.query(`UPDATE user_ideas SET status = "disapproved" WHERE id = ${idea_id}`);
    } catch (err) {
      return {
        disapproved: false,
        msg: err.sqlMessage,
      };
    }
    if (disapproved1.affectedRows > 0 && disapproved2.affectedRows > 0) {
      return {
        disapproved: true,
        msg: "Idea has been disapproved",
      };
    }
    return {
      disapproved: false,
      msg: "Something went wrong. Idea could not be disapproved",
    };
  },
};
