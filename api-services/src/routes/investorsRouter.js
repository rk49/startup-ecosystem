const express = require('express');
const { getInvestors } = require('../controller/investorsController');

const router = express.Router();

router.post('/getInvestors', async (req, res) => {
  const invObj = await getInvestors();
  if (invObj.fetched) {
    res.status(200).json({ ...invObj });
  } else {
    res.status(500).json({ ...invObj });
  }
});

module.exports = router;
