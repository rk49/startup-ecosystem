const express = require('express');
const { getUserById } = require('../controller/usersController');

const router = express.Router();

router.post('/getUserById', async (req, res) => {
  const { uid } = req.body;
  const user = await getUserById(uid);
  if (user.fetched) {
    res.status(200).json({ ...user });
  } else {
    res.status(500).json({ ...user });
  }
});

module.exports = router;
