const express = require('express');
const {
  registerIdea, submitIdea, getIdeas, getIdeasById, getIdeasByInvId,
  approveIdea, disapproveIdea, deleteIdea,
} = require('../controller/ideasController');

const router = express.Router();

router.post('/getIdeas', async (req, res) => {
  const ideasObj = await getIdeas();
  if (ideasObj.fetched) {
    res.status(200).json({
      ideas: ideasObj.ideas,
      msg: ideasObj.msg,
    });
  } else {
    res.status(500).json({ msg: ideasObj.msg });
  }
});

router.post('/getIdeas/:id', async (req, res) => {
  const uid = req.params.id;
  const ideasObj = await getIdeasById(uid);
  if (ideasObj.fetched) {
    res.status(200).json({
      ideas: ideasObj.ideas,
      msg: ideasObj.msg,
    });
  } else {
    res.status(500).json({ msg: ideasObj.msg });
  }
});

router.post('/getIdeasByInvId', async (req, res) => {
  const { inv_uid } = req.body;
  const ideasObj = await getIdeasByInvId(inv_uid);
  if (ideasObj.fetched) {
    res.status(200).json({
      ...ideasObj,
    });
  } else {
    res.status(500).json({ ...ideasObj });
  }
});

router.post('/deleteIdea', async (req, res) => {
  const { idea_id } = req.body;
  const ideasObj = await deleteIdea(idea_id);
  if (ideasObj.deleted) {
    res.status(200).json({
      ...ideasObj,
    });
  } else {
    res.status(500).json({ ...ideasObj });
  }
});

router.post('/registerIdea', async (req, res) => {
  const ideaInfo = req.body;
  const ideasObj = await registerIdea(ideaInfo);
  if (ideasObj.inserted) {
    res.status(200).json({ ...ideasObj });
  } else {
    res.status(500).json({ ...ideasObj });
  }
});

router.post('/submitIdea', async (req, res) => {
  const ideaInfo = req.body;
  const ideasObj = await submitIdea(ideaInfo);
  if (ideasObj.inserted) {
    res.status(200).json({ ...ideasObj });
  } else {
    res.status(500).json({ ...ideasObj });
  }
});

router.post('/approveIdea', async (req, res) => {
  const ideaInfo = req.body;
  const ideasObj = await approveIdea(ideaInfo);
  if (ideasObj.approved) {
    res.status(200).json({ ...ideasObj });
  } else {
    res.status(500).json({ ...ideasObj });
  }
});

router.post('/disapproveIdea', async (req, res) => {
  const ideaInfo = req.body;
  const ideasObj = await disapproveIdea(ideaInfo);
  if (ideasObj.disapproved) {
    res.status(200).json({ ...ideasObj });
  } else {
    res.status(500).json({ ...ideasObj });
  }
});

module.exports = router;
