const express = require('express');

const home = express.Router();

home.get('/', (req, res) => {
  res.render('homepage');
});

home.get('/home', (req, res) => {
  res.render('homepage');
});

module.exports = home;
