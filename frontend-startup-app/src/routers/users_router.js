const express = require('express');
const passport = require('passport');
const axios = require('axios');
const checkAuth = require('../common/routes_middleware');
const authController = require('../controllers/auth/authController');
const userController = require('../controllers/user/userController');
const getIdeas = require('../controllers/user/getIdeas');
const { ec2URL } = require('../config/config');

const key = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IkhleSEgV2VsY29tZSBTdGFydCBVc2luZyBvdXIgQXBpLiIsImlhdCI6MTUzOTE0MjIyOCwiZXhwIjoyNDAzMTQyMjI4fQ.CmafDmYt2Xwdmv-UXNjY8L0qAPrZn3c9mmk2Lopjarc';


const users = express.Router();

users.get('/', (req, res) => {
  if (!req.user) {
    res.render('users/users_home');
  } else {
    res.redirect('users/user');
  }
});

users.get('/user', checkAuth, async (req, res) => {
  const uuid = req.user[0];
  const result = await getIdeas(uuid);
  res.render('users/user_dashboard', { uuid, result });
});

users.get('/registerdetails/:id', (req, res) => {
  const uuid = req.params.id;
  res.render('users/user_registration', { uuid });
});

users.post('/register/:id', async (req, res) => {
  const uuid = req.params.id;
  const details = req.body;
  try {
    const result = await userController.registerDetails(uuid, details);
    if (result.data.inserted) {
      const info = {
        uid: uuid,
        msg: 'OK',
        token: key,
        table: 'user_reg',
      };
      await axios.put(`${ec2URL}:5003/activate`, info);
      res.redirect('/users');
    } else {
      res.send('registration Failed');
    }
    console.log(result);
  } catch (error) {
    console.log(error);
  }
});

users.get('/user/ideas', checkAuth, (req, res) => {
  const uuid = req.user[0];
  res.render('users/user_dashboard', { uuid });
});

users.post('/signup', async (req, res) => {
  const user = req.body;
  const regUser = await authController.regUser(user);
  console.log(regUser);
  res.render('users/users_home');
});


users.post('/login', passport.authenticate('local',
  { successRedirect: '/users/user', failureRedirect: '/users' }), (req, res) => {
  res.redirect('/home');
});

users.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/users');
});
module.exports = users;
