const axios = require('axios');

const { ec2URL } = require('../../config/config');

const key = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IkhleSEgV2VsY29tZSBTdGFydCBVc2luZyBvdXIgQXBpLiIsImlhdCI6MTUzOTE0MjIyOCwiZXhwIjoyNDAzMTQyMjI4fQ.CmafDmYt2Xwdmv-UXNjY8L0qAPrZn3c9mmk2Lopjarc';
async function regUser(user) {
  const userDetails = {
    username: user.username,
    password: user.password,
    confirm_password: user.confirmPassword,
    email: user.email,
    token: key,
    table: 'user_reg',
  };
  try {
    const url = `${ec2URL}:5003/register`;
    const result = await axios.put(url, userDetails);
    console.log(result);
  } catch (error) {
    console.log(error);
  }
}

module.exports.regUser = regUser;
