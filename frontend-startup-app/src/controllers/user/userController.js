const axios = require('axios');

const { ec2URL } = require('../../config/config');

async function registerDetails(uuid, details) {
  const insert = {
    uid: uuid,
    first_name: details.first_name,
    last_name: details.last_name,
    email: details.email,
    sex: details.sex,
    address: details.address,
    country: details.country,
    state: details.state,
    city: details.city,
    dob: details.dob,
    pan: details.pan,
    aadhaar: details.aadhaar,
    phone_no: details.phone_no,
    pincode: details.pincode,
    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw',
  };
  let result;
  try {
    result = await axios.post(`${ec2URL}:3001/register/registerUser`, insert);
    console.log(result);
  } catch (err) {
    console.log(err);
  }
  return result;
}

module.exports.registerDetails = registerDetails;
