const axios = require('axios');
const { ec2URL } = require('../../config/config');

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw';
module.exports = async function getideas(uid) {
  try {
    const result = await axios.default.post(`${ec2URL}:4000/ideas/getIdeas/${uid}`, { token });
    return result.data;
  } catch (error) {
    return error;
  }
};
