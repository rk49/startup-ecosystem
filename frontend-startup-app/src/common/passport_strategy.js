const axios = require('axios');
const LocalStrategy = require('passport-local').Strategy;

const { ec2URL } = require('../config/config');

const key = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IkhleSEgV2VsY29tZSBTdGFydCBVc2luZyBvdXIgQXBpLiIsImlhdCI6MTUzOTE0MjIyOCwiZXhwIjoyNDAzMTQyMjI4fQ.CmafDmYt2Xwdmv-UXNjY8L0qAPrZn3c9mmk2Lopjarc';


function passportConfig(passport) {
  try {
    passport.use('local', new LocalStrategy(
      async (username, password, done) => {
        console.log('hereeee');
        try {
          console.log(username + password);
          const body = {
            username, password, table: 'user_reg', token: key,
          };
          console.log(body);
          const result = await axios.post(`${ec2URL}:5003/login`, body);
          let user = result.data.msg;
          user = user.split(',');
          console.log(user);
          console.log('here');
          if (user[0] === 'Invalid User') {
            return done(null, false);
          }
          return done(null, user);
        } catch (err) {
          return done(err, false);
        }
      },
    ));
  } catch (error) {
    console.log(error);
  }


  passport.serializeUser((username, done) => {
    if (!username) {
      done(null, false);
    }
    console.log(`${username} logged in`);
    done(null, username);
  });

  passport.deserializeUser(async (username, done) => {
    if (!username) {
      done(null, false);
    } else {
      try {
        done(null, username);
      } catch (err) {
        console.log(err);
        done(err, username);
      }
    }
  });
}

module.exports = passportConfig;
