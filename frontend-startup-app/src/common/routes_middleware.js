
function checkAuth(req, res, next) {
  if (!req.user) {
    res.redirect('/users');
  } else if (req.user[1] === 'unauthenticated') {
    const uuid = (req.user[0]);
    req.logout();
    res.redirect(`/users/registerdetails/${uuid}`);
  } else {
    next();
  }
}
module.exports = checkAuth;
