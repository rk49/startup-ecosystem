const express = require('express');
const passport = require('passport');
const session = require('express-session');
const bodyParser = require('body-parser');

const users = require('../src/routers/users_router');
const homepage = require('../src/routers/home_routes');
const passportConfig = require('../src/common/passport_strategy');

const app = express();
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));
app.use(session({ secret: 'yefaydfcavdfva3210d' }));
passportConfig(passport);
app.use(passport.initialize());
app.use(passport.session());

app.use('/', homepage);
app.use('/users', users);

app.listen(3000, () => {
  console.log('running on 3000');
});
