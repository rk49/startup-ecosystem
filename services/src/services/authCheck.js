const jwt = require('jsonwebtoken');
const { JWT_KEY } = require('../config/config');

module.exports = (req, res, next) => {
  try {
    const decoded = jwt.verify(req.body.token, JWT_KEY);
    req.userData = decoded;
    next();
    return true;
  } catch (err) {
    return res.status(401).json({
      message: 'Auth failed',
    });
  }
};
