const express = require('express');
const { registerInvestor, registerUser } = require('../controller/registerController');

const router = express.Router();

router.post('/registerInvestor', async (req, res) => {
  const userInfo = req.body;
  const invObj = await registerInvestor(userInfo);
  if (invObj.inserted) {
    res.status(200).json({
      ...invObj,
    });
  } else {
    res.status(500).json({
      ...invObj,
    });
  }
});

router.post('/registerUser', async (req, res) => {
  const userInfo = req.body;
  const userObj = await registerUser(userInfo);
  if (userObj.inserted) {
    res.status(200).json({
      ...userObj,
    });
  } else {
    res.status(500).json({
      ...userObj,
    });
  }
});

module.exports = router;
