const pool = require('../config/connection');

module.exports = {
  registerInvestor: async (userInfo) => {
    const {
      inv_uid, inv_first, inv_last, inv_email, inv_sex,
      inv_country, inv_state, inv_city, inv_pincode,
      inv_linkedin, inv_interest, inv_investment, inv_phone_no,
      inv_pan, inv_aadhaar, inv_dob, inv_pic,
    } = userInfo;
    console.log(inv_uid, inv_first, inv_last, inv_email,
      inv_sex, inv_country, inv_state, inv_city, inv_pincode,
      inv_linkedin, inv_interest, inv_investment, inv_phone_no, inv_pan,
      inv_aadhaar, inv_dob, inv_pic);
    try {
      const inserted = await pool.query(`INSERT INTO investors(id, inv_uid, inv_first, inv_last, inv_email, inv_sex, 
        inv_country, inv_state, inv_city, inv_pincode, inv_linkedin, inv_interest, inv_investment, inv_phone_no, 
        inv_pan, inv_aadhaar, inv_dob, inv_pic) VALUES (null, "${inv_uid}", "${inv_first}", "${inv_last}", "${inv_email}", "${inv_sex}", 
        "${inv_country}", "${inv_state}", "${inv_city}", "${inv_pincode}", "${inv_linkedin}", "${inv_interest}", "${inv_investment}", "${inv_phone_no}", 
        "${inv_pan}", "${inv_aadhaar}", "${inv_dob}", "${inv_pic}")`);
      if (inserted) {
        return {
          inserted: true,
          msg: "Investor successfully registered",
        };
      }
      return {
        inserted: false,
        msg: "Something went wrong, the investor could not be registered",
      };
    } catch (err) {
      return {
        inserted: false,
        msg: err.sqlMessage,
      };
    }
  },
  registerUser: async (userInfo) => {
    const {
      uid, first_name, last_name, email, sex, address,
      country, state, city, dob, pan, aadhaar, phone_no, pincode,
    } = userInfo;
    console.log(uid, first_name, last_name, email, sex, address,
      country, state, city, dob, pan, aadhaar, phone_no, pincode);
    try {
      const inserted = await pool.query(`INSERT INTO users(id, uid, first_name, last_name, email, sex, address,
        country, state, city, dob, pan, aadhaar, phone_no, pincode) VALUES (null, "${uid}", "${first_name}", "${last_name}", 
        "${email}", "${sex}", "${address}", "${country}", "${state}", "${city}", "${dob}", "${pan}", "${aadhaar}", ${phone_no}, 
        "${pincode}")`);
      console.log(inserted);
      if (inserted) {
        return {
          inserted: true,
          msg: "User successfully registered",
        };
      }
      return {
        inserted: false,
        msg: "Something went wrong, the user could not be registered",
      };
    } catch (err) {
      return {
        inserted: false,
        msg: err.sqlMessage,
      };
    }
  },
};
