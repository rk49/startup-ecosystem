const mysql = require('mysql2');
const util = require('util');
const {
  hostValue, testHost, user, testPassword, passwordValue, defaultDatabase,
} = require('../config/config');

let host; let password; let database;
if (process.env.NODE_ENV === 'testing') {
  host = testHost; password = testPassword; database = process.env.USE_TEST_DB;
} else {
  host = hostValue; password = passwordValue; database = defaultDatabase;
}

console.log("Database = ", database);

const pool = mysql.createPool({
  connectionLimit: 1000,
  host,
  user,
  password,
  database,
});
pool.query = util.promisify(pool.query);

module.exports = pool;
