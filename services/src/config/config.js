module.exports = {
  hostValue: 'rkenterprises.clclggqqwjvk.us-east-2.rds.amazonaws.com',
  testHost: 'rkenterprises.clclggqqwjvk.us-east-2.rds.amazonaws.com',
  user: 'root',
  testPassword: 'password',
  passwordValue: 'password',
  defaultDatabase: 'startup_ecosystem',
  JWT_ID: 'investorServiceID',
  JWT_KEY: 'investorServiceKey',
  JWT_TOKEN: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw',
};
