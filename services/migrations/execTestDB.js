const pool = require('../src/config/connection');

async function execDB() {
  const invDeleted = await pool.query('DELETE FROM investors');
  const usrDeleted = await pool.query('DELETE FROM users');
  if (invDeleted && usrDeleted) {
    console.log('Tables delted');
    pool.end();
  }
}

execDB();
