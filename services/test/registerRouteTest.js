const request = require('supertest');
const app = require('../server');
const { JWT_TOKEN } = require('../src/config/config');

require('./registerControllerTest');

const invObj = {
    inv_uid: "uid2",
    inv_first: "first_name2",
    inv_last: "last_name2",
    inv_email: "email2",
    inv_sex: "male",
    inv_country: "India",
    inv_state: "Karnataka",
    inv_city: "Bengaluru",
    inv_pincode: 560003,
    inv_linkedin: "jay@linkedin.io",
    inv_interest: "IT",
    inv_investment: "55,000",
    inv_phone_no: 850822125,
    inv_pan: "pan_number",
    inv_aadhaar: "Aadhar number",
    inv_dob: "23/07/15",
    inv_pic: "profile_pic",
    token: JWT_TOKEN,
  };
  
  const userObj = {
    uid: "uid2",
    first_name: "first2", 
    last_name: "last2", 
    email: "email2", 
    sex: "male", 
    address: "address",
    country: "India", 
    state: "Karnataka", 
    city: "Bengaluru", 
    dob: "23-07-15", 
    pan: "pan2", 
    aadhaar: "aadhar2", 
    phone_no: 391333439, 
    pincode: 23213,
    token: JWT_TOKEN
  };

describe('Basic Supertest for login route', () => {
  it('should be able to access protected login/loginInvestor route ', function (done) {
    this.timeout(10000);
    request(app)
      .post('/register/registerInvestor')
      .send(invObj)
      .expect(200)
      .end(done);
  });

  it('should be able redirected for wrong URL', function (done) {
    this.timeout(10000);
    request(app)
      .post('/register/wrongURL')
      .send(invObj)
      .expect(404)
      .end(done);
  });

  it('should return status 500 for duplicate investor credentials', function (done) {
    this.timeout(10000);
    request(app)
      .post('/register/registerInvestor')
      .send(invObj)
      .expect(500)
      .end(done);
  });

  it('should be able to access protected /register/registerUser route ', function (done) {
    this.timeout(10000);
    request(app)
      .post('/register/registerUser')
      .send(userObj)
      .expect(200)
      .end(done);
  });

  it('should be able redirected for wrong user URL', function (done) {
    this.timeout(10000);
    request(app)
      .post('/register/wrongURL')
      .send(userObj)
      .expect(404)
      .end(done);
  });

  it('should return status 500 for duplicate user credentials', function (done) {
    this.timeout(10000);  
    request(app)
      .post('/register/registerUser')
      .send(userObj)
      .expect(500)
      .end(done);
  });
});
