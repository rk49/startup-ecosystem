const { expect } = require('chai');
const { registerInvestor, registerUser } = require('../src/controller/registerController');

const invObj = {
  inv_uid: "uid1",
  inv_first: "first_name",
  inv_last: "last_name",
  inv_email: "email1",
  inv_sex: "male",
  inv_country: "India",
  inv_state: "Karnataka",
  inv_city: "Bengaluru",
  inv_pincode: 560003,
  inv_linkedin: "jay@linkedin.io",
  inv_interest: "IT",
  inv_investment: "55,000",
  inv_phone_no: 850812125,
  inv_pan: "pan_number",
  inv_aadhaar: "Aadhar number",
  inv_dob: "23/07/15",
  inv_pic: "profile_pic",
};

const userObj = {
  uid: "uid1",
  first_name: "first", 
  last_name: "last", 
  email: "email1", 
  sex: "male", 
  address: "address",
  country: "India", 
  state: "Karnataka", 
  city: "Bengaluru", 
  dob: "23-07-15", 
  pan: "pan", 
  aadhaar: "aadhar", 
  phone_no: 398333439, 
  pincode: 23213,
};

describe('Basic MOCHA test for register controller', function () {
  it('should return true for correct credentials', async function () {
    this.timeout(10000);
    let result = false;
    const expectedResult = true;
    const resultValue = await registerInvestor(invObj);
    if (resultValue.inserted) result = true;
    expect(result).deep.equals(expectedResult);
  });

  it('should return false for same credentials', async function () {
    this.timeout(10000);
    let result = false;
    const expectedResult = false;
    const resultValue = await registerInvestor(invObj);
    if (resultValue.inserted) result = true;
    expect(result).deep.equals(expectedResult);
  });

  it('should return true for correct credentials', async function () {
    this.timeout(10000);
    let result = false;
    const expectedResult = true;
    const resultValue = await registerUser(userObj);
    if (resultValue.inserted) result = true;
    expect(result).deep.equals(expectedResult);
  });

  it('should return false for same credentials', async function () {
    this.timeout(10000);
    let result = false;
    const expectedResult = false;
    const resultValue = await registerUser(userObj);
    if (resultValue.inserted) result = true;
    expect(result).deep.equals(expectedResult);
  });
});


