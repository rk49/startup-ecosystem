Initial instruction - 
1)npm install -g nodemon
2)npm run lint-setup
3)npm install 
4)npm start



Available APIS - 
 (Dev: host = localhost, PORT = 3000) 

a)Registration APIS
 1)"http://HOST:PORT/register/registerInvestor" - Registers the investor to the DB

 Description: 
 -> POST request
 -> Sample request object = {
  "inv_uid": "uid3", 
  "inv_first": "first_name", 
  "inv_last": "last_name", 
  "inv_email": "email3", 
  "inv_sex": "male",
  "inv_country": "India", 
  "inv_state": "Karnataka", 
  "inv_city": "Bengaluru", 
  "inv_pincode": 560003, 
  "inv_linkedin": "jay@linkedin.io", 
  "inv_interest": "IT",
  "inv_investment": "55,000", 
  "inv_phone_no": 890832025,
  "inv_pan": "pan_number", 
  "inv_aadhaar": "Aadhar number", 
  "inv_dob": "23/07/15", 
  "inv_pic": "profile_pic",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw"
}
-> Sample response = {
    "inserted": true,
    "msg": "Investor successfully registered"
}
-> RESPONSE STATUS CODES - 200, 500, 404

 
 2)"http://HOST:PORT/register/registerUser" - Registers the user to the DB

 Description: 
 -> POST request
 -> Sampple request object = {
	"uid": "uid2", 
	"first_name": "first", 
	"last_name": "last", 
	"email": "email2", 
	"sex": "male", 
	"address": "address",
    "country": "India", 
    "state": "Karnataka", 
    "city": "Bengaluru", 
    "dob": "23-07-15", 
    "pan": "pan", 
    "aadhaar": "aadhar", 
    "phone_no": 439834439, 
    "pincode": 23213,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImludmVzdG9yU2VydmljZUlEIiwiaWF0IjoxNTM5MzEwODA5LCJleHAiOjE1NDA2MDY4MDl9._xgB_R5Tac2Y_t6JxzlSS-iwtzMKmPmF-4w-lSODuJw"
}
-> Sample response = {
    "inserted": true,
    "msg": "User successfully registered"
}
-> RESPONSE STATUS CODES - 200, 500, 404

_____________________________________________________________________________________________________
