const express = require('express');
const bodyParser = require('body-parser');
const tokenChecker = require('./src/services/authCheck');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));

const registerRoute = require('./src/routes/registerRoute');

const PORT = 3001;

app.use('/register', tokenChecker, registerRoute);

app.listen(PORT, () => console.log(`App listening on port ${PORT}`));

module.exports = app;
